package org.rlabs.gdg.helloworld;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Main extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Info: At a start of any Android Application is the 'OnCreate'
		// function. This is where apps begins their execution.
		super.onCreate(savedInstanceState);
		// You can to let it load a view
		setContentView(R.layout.main);
		// Info: We create our first function to do more code for us
		setupView();
	}

	private void setupView() {
		// Info: The code will continue from here

		// This button button links what is on your screen, the button you see
		// with something called a Listener.
		// A listener is a class in java that wait for something to happens, but
		// you got to ask it to listen to something first.
		Button button1 = (Button) findViewById(R.id.button1);
		// Here were asking the listener to listen for a button press
		button1.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// Before pulling the text to display it on the screen, we need Java to
		// link up the XML (whats displayed) with something Java can utilze.
		EditText editText1 = (EditText) findViewById(R.id.editText1);
		// Here we storing what the you typed in a variable called a 'string'. A
		// 'string' holds a list of character; in this case its holding the
		// words someone typed in.
		String editText_string = editText1.getText().toString();

		// Here once again we registering a TextView. A TextView is simple
		// something that displays on the screen words - Strings if you were
		// following.
		TextView textView1 = (TextView) findViewById(R.id.textView1);
		// Here we telling the TextView to display the text - held in
		// 'editText_string' into the TextView below.
		textView1.setText("You typed: " + editText_string);
	}

}
